# DFTAnalysis

A python library for handling(create and read), manipulating, and analyzing output from DFT programs: GPAW, and VASP.  It solves some of the multi-step processes by creating a unified scripting approach in Python. 